package com.solonarv.mods.toughlamps;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = "tlamps", name = "Tough Lamps", version = "1.6.2")
public class ToughLamps {
    @Instance
    public static ToughLamps          instance;
    
    public static BlockToughGlowstone toughGlowstone;
    public static int                 toughGlowstoneID;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent e) {
        
        toughGlowstone = (BlockToughGlowstone) (new BlockToughGlowstone()
                .setBlockName("toughGlowstone").setHardness(1.5f)
                // enough for point-blank charged creeper
                .setResistance(125).setStepSound(Block.soundTypeMetal)
                .setCreativeTab(CreativeTabs.tabBlock));
        GameRegistry.registerBlock(toughGlowstone, BlockToughGlowstone.CustomItemBlock.class, "toughGlowstone");
        
        
        /*
         * make toughened glowstone: 3 glowstone in the middle row/col,
         * surrounded by 6 iron. Gives 4.
         */
        GameRegistry.addRecipe(new ItemStack(toughGlowstone, 4, 0),
                new Object[] { "igi", "igi", "igi", 'i', Items.iron_ingot, 'g',
                        Blocks.glowstone });
        GameRegistry.addRecipe(new ItemStack(toughGlowstone, 4, 0),
                new Object[] { "iii", "ggg", "iii", 'i', Items.iron_ingot, 'g',
                        Blocks.glowstone });
        // convert to a toughened lamp: shapeless with redstone
        GameRegistry.addShapelessRecipe(new ItemStack(toughGlowstone, 1, 1),
                Items.redstone, Items.redstone, new ItemStack(toughGlowstone, 1,
                        0));
        // convert between inverted/non-inverted lamps by just crafting
        GameRegistry.addShapelessRecipe(new ItemStack(toughGlowstone, 1, 3),
                new ItemStack(toughGlowstone, 1, 1));
        GameRegistry.addShapelessRecipe(new ItemStack(toughGlowstone, 1, 1),
                new ItemStack(toughGlowstone, 1, 3));
    }
}
