package com.solonarv.mods.toughlamps;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockToughGlowstone extends Block {
    
    protected IIcon[]   icons;
    protected String[] names = { "toughGlowstone", "toughRSLamp_off",
            "toughRSLamp_on", "invToughRSLamp_on", "invToughRSLamp_off" };
    
    public BlockToughGlowstone() {
        super(Material.rock);
    }
    
    @Override
    public IIcon getIcon(int side, int meta) {
        return meta < 3 ? this.icons[meta] : this.icons[5 - meta];
    }
    
    @Override
    public void registerBlockIcons(IIconRegister ir) {
        this.icons = new IIcon[] { ir.registerIcon("tlamp:toughGlowstone"),
                ir.registerIcon("tlamp:toughRSLamp_off"),
                ir.registerIcon("tlamp:toughRSLamp_on") };
    }
    
    @Override
    public int damageDropped(int meta) {
        return meta == 2 ? 1 : meta == 4 ? 3 : meta;
    }
    
    @Override
    public int getDamageValue(World world, int x, int y, int z) {
        return this.damageDropped(world.getBlockMetadata(x, y, z));
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public void getSubBlocks(Item item, CreativeTabs tab, List subBlocks) {
        subBlocks.add(new ItemStack(item, 1, 0)); // glowstone
        subBlocks.add(new ItemStack(item, 1, 1)); // lamp
        subBlocks.add(new ItemStack(item, 1, 3)); // inv. lamp
    }
    
    @Override
    public int getLightValue(IBlockAccess world, int x, int y, int z) {
        switch (world.getBlockMetadata(x, y, z)) {
            case 0: // It's glowstone
            case 2: // It's a lamp (on)
            case 3: // It's an inverted lamp (on)
                return 15;
            case 1: // It's a lamp (off)
            case 4: // It's an inverted lamp (off)
                return 0;
        }
        return 0;
    }
    
    protected void updateOnRedstone(World world, int x, int y, int z) {
        int meta = world.getBlockMetadata(x, y, z);
        if (meta == 1 && world.isBlockIndirectlyGettingPowered(x, y, z)) {
            world.setBlockMetadataWithNotify(x, y, z, 2, 3);
        } else if (meta == 2 && !world.isBlockIndirectlyGettingPowered(x, y, z)) {
            world.setBlockMetadataWithNotify(x, y, z, 1, 3);
        } else if (meta == 3 && world.isBlockIndirectlyGettingPowered(x, y, z)) {
            world.setBlockMetadataWithNotify(x, y, z, 4, 3);
        } else if (meta == 4 && !world.isBlockIndirectlyGettingPowered(x, y, z)) {
            world.setBlockMetadataWithNotify(x, y, z, 3, 3);
        }
    }
    
    @Override
    public void onBlockAdded(World world, int x, int y, int z) {
        if (!world.isRemote) {
            this.updateOnRedstone(world, x, y, z);
        }
    }
    
    @Override
    public void onNeighborBlockChange(World world, int x, int y, int z, Block neighbor) {
        if (!world.isRemote) {
            this.updateOnRedstone(world, x, y, z);
        }
    }
    
    public static class CustomItemBlock extends ItemBlock{

		public CustomItemBlock(Block blk) {
			super(blk);
			this.setHasSubtypes(true);
			this.setMaxDamage(0);
		}
		
		@Override
		public int getMetadata(int meta) {
	        return meta;
	    }
    	
    }
}
